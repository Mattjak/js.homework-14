$(document).ready(function () {
    $(`.tabs-menu-item`).click(function () {
        switchTabs(this, "active", "tabs-content-item");
    });

    function switchTabs(elem, switchClass, contentClass) {
        $(elem).addClass(switchClass).siblings().removeClass(switchClass);
        const indexOfTab = $(elem).index();
        $(`.${contentClass}`).removeClass(switchClass).eq(indexOfTab).addClass(switchClass);
    }
});